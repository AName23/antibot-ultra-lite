package aname.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import aname.configuration.ConfigManagement;
import aname.core.AntiBotUltraLite;
import aname.utils.TranslateAlternateUtils;
import aname.utils.WhitelistUtils;

public class Events implements Listener {

	private WhitelistUtils whitelist = new WhitelistUtils();
	private ConfigManagement config = new ConfigManagement();
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (config.getConfig().getBoolean("Settings.clear-join")) {
			event.setJoinMessage(null);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerQuit(PlayerQuitEvent event) {
		if (config.getConfig().getBoolean("Settings.clear-join")) {
			event.setQuitMessage(null);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		if (!whitelist.isOnWhitelist(player)) {
			int value = AntiBotUltraLite.hBot.get("LoginCount");
			AntiBotUltraLite.hBot.put("LoginCount", value + 1);
			new BukkitRunnable() {
				@Override
				public void run() {
					whitelist.addPlayerToWhitelist(player);
				}
			}.runTaskLater(AntiBotUltraLite.instance, config.getConfig().getLong("Settings.whitelist-check-delay"));
			if (whitelist.isWhitelistActive()) {
				event.setResult(Result.KICK_OTHER);
				event.setKickMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.whitelist-kick").replaceAll("%new_line%", System.lineSeparator())));
			}
		}
	}
}
