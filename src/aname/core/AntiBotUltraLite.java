package aname.core;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.Metrics;

import aname.configuration.ConfigManagement;
import aname.events.Events;
import aname.utils.TranslateAlternateUtils;
import aname.utils.WhitelistUtils;

public class AntiBotUltraLite extends JavaPlugin {

	public static AntiBotUltraLite instance;

	private ConfigManagement config = new ConfigManagement();
	private WhitelistUtils whitelist = new WhitelistUtils();

	public static HashMap<String, Integer> hBot = new HashMap<String, Integer>();

	private String version = Bukkit.getServer().getClass().getPackage().getName().replace(".",  ",").split(",")[3];

	@Override
	public void onEnable() {
		if (!version.equals("v1_7_R1") || !version.equals("v1_7_R2") || !version.equals("v1_7_R3")) {
			instance = this;
			hBot.put("LoginCount", 1);
			if (!new File(getDataFolder(), "whitelist.yml").exists()) {
				saveResource("whitelist.yml", false);
			}
			config.loadConfig();
			getServer().getPluginManager().registerEvents(new Events(), this);
			metrics();
			protection();
		} else {
			this.getLogger().info("ERROR! HALT HALT HALT");
			this.getLogger().info(version + " is not supported!");
			Bukkit.getPluginManager().disablePlugin(this);
		}
	}

	private void metrics() {
		try {
			Metrics metrics = new Metrics();
			metrics.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void protection() {
		if (config.getConfig().getBoolean("Settings.whitelist")) {
			getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
				@Override
				public void run() {
					if (hBot.get("LoginCount") < config.getConfig().getInt("Settings.sensibility")) {
						hBot.put("LoginCount", 0);
					} else if (hBot.get("LoginCount") > config.getConfig().getInt("Settings.sensibility") && !whitelist.isWhitelistActive()) {
						whitelist.setWhitelistActive(true);
						for (Player p : Bukkit.getOnlinePlayers()) {
							if (p.hasPermission("Antibot.spy") || p.isOp()) {
								p.sendMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.attack-detected")));
							}
						}
						getServer().getScheduler().runTaskLater(AntiBotUltraLite.instance, new Runnable() {
							@Override
							public void run() {
								hBot.put("LoginCount", 0);
								for (Player p : Bukkit.getOnlinePlayers()) {
									if (p.hasPermission("Antibot.spy") || p.isOp()) {
										p.sendMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.attack-analize")));
									}
								}
							}
						}, 2000L);
						getServer().getScheduler().runTaskLater(AntiBotUltraLite.instance, new Runnable() {
							@Override
							public void run() {
								if (hBot.get("LoginCount") < config.getConfig().getInt("Settings.sensibility")) {
									whitelist.setWhitelistActive(false);
									hBot.put("LoginCount", 0);
									for (Player p : Bukkit.getOnlinePlayers()) {
										if (p.hasPermission("Antibot.spy") || p.isOp()) {
											p.sendMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.attack-finish")));
										}
									}
								} else {
									for (Player p : Bukkit.getOnlinePlayers()) {
										if (p.hasPermission("Antibot.spy") || p.isOp()) {
											p.sendMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.attack-persist")));
										}
									}
									whitelist.setWhitelistActive(false);
								}
							}
						}, 3000L);
					}
				}
			}, 100L, 100L);
		}
	}
}
