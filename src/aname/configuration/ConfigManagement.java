package aname.configuration;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import aname.core.AntiBotUltraLite;

public class ConfigManagement {
	
	private File configFile;
	private FileConfiguration config;
	
	private File whitelistFile;
	private FileConfiguration whitelist;
	
	public void reloadWhitelist() {
		if (whitelistFile == null) {
			whitelistFile = new File(AntiBotUltraLite.instance.getDataFolder(), "config.yml");
		}
		config = YamlConfiguration.loadConfiguration(configFile);
	}
	
	public FileConfiguration getWhitelist() {
		if (whitelist == null) {
			reloadWhitelist();
		}
		return whitelist;
	}
	
	public void saveWhitelist() {
		if ((whitelist == null) || (whitelistFile == null)) {
			return;
		} try {
			whitelist.save(whitelistFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void reloadConfig() {
		if (configFile == null) {
			configFile = new File(AntiBotUltraLite.instance.getDataFolder(), "config.yml");
		}
		config = YamlConfiguration.loadConfiguration(configFile);
	}
	
	public FileConfiguration getConfig() {
		if (config == null) {
			reloadConfig();
		}
		return config;
	}
	
	public void saveConfig() {
		if ((config == null) || (configFile == null)) {
			return;
		} try {
			config.save(configFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void loadConfig() {
		this.getConfig().addDefault("AntiBotUltraLite.Version", 1.0);
		this.getConfig().addDefault("Settings.clear-join", true);
		this.getConfig().addDefault("Settings.sensibility", 3);
		this.getConfig().addDefault("Settings.whitelist", true);
		this.getConfig().addDefault("Settings.whitelist-add-delay", 12000);
		this.getConfig().addDefault("Settings.whitelist-check-delay", 720);
		
		this.getConfig().addDefault("Messages.whitelist-add", "&7[&cAntiBot&7-&cUltra&7] &d%player% &ahas been added to the whitelist!");
		this.getConfig().addDefault("Messages.whitelist-remove", "&7[&cAntiBot&7-&cUltra&7] &d%player% &ahas been removed from the whitelist!");
		this.getConfig().addDefault("Messages.whitelist-kick", "&7[&cAntiBot&7-&cUltra&7]&r%new_line%&aYou have been kicked!%new_line%&cReason: &4You are not on the whitelist!");
		this.getConfig().addDefault("Messages.attack-detected", "&7[&cAntiBot&7-&cUltra&7] &dBot attack detected, whitelist on!");
		this.getConfig().addDefault("Messages.attack-finish", "&7[&cAntiBot&7-&cUltra&7] &dBot attack finished, whitelist off!");
		this.getConfig().addDefault("Messages.attack-analize", "&7[&cAntiBot&7-&cUltra&7] &dAnalizing bot attack...");
		this.getConfig().addDefault("Messages.attack-persist", "&7[&cAntiBot&7-&cUltra&7] &dBot attack persist! Restarting counter...");
		this.getConfig().options().copyDefaults(true);
		this.saveConfig();
	}

}
