package aname.utils;

import org.bukkit.ChatColor;

public class TranslateAlternateUtils {
	
	public static String format(String format) {
		return ChatColor.translateAlternateColorCodes('&', format);
	}
	
}
