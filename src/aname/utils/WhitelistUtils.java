package aname.utils;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import aname.configuration.ConfigManagement;
import aname.core.AntiBotUltraLite;

public class WhitelistUtils {
	
	private ConfigManagement config = new ConfigManagement();
	private boolean whitelistActive = false;
	
	public boolean isOnWhitelist(Player player) {
		if (config.getConfig().getBoolean("Settings.whitelist")) {
			for (String playerCfg : config.getWhitelist().getStringList("Whitelisted-Players")) {
				if (playerCfg.equalsIgnoreCase(player.getName())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean addPlayerToWhitelist(Player player) {
		if (config.getConfig().getBoolean("Settings.whitelist")) {
			List<String> players = config.getWhitelist().getStringList("Whitelisted-Players");
			Bukkit.getScheduler().runTaskLater(AntiBotUltraLite.instance, new Runnable() {
				@Override
				public void run() {
					if (!isOnWhitelist(player) && player.isOnline()) {
						players.add(player.getName());
						config.getWhitelist().set("Whitelisted-Players", players);
						for (Player onPlayers : Bukkit.getOnlinePlayers()) {
							if (onPlayers.isOp() || onPlayers.hasPermission("Antibot.spy")) {
								onPlayers.sendMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.whitelist-add").replace("%player%", player.getName())));
							}
						}
					} else if (!isOnWhitelist(player) && !player.isOnline() && players.contains(player)) {
						players.remove(player.getName());
						config.getWhitelist().set("Whitelisted-Players", players);
						config.saveWhitelist();
						for (Player onPlayers : Bukkit.getOnlinePlayers()) {
							if (onPlayers.isOp() || onPlayers.hasPermission("Antibot.spy")) {
								onPlayers.sendMessage(TranslateAlternateUtils.format(config.getConfig().getString("Messages.whitelist-remove").replace("%player%", player.getName())));
							}
						}
					}
				}
			}, config.getConfig().getLong("Settings.whitelist-add-delay"));
		}
		return false;
	}
	
	public boolean isWhitelistActive() {
		return whitelistActive;
	}
	
	public void setWhitelistActive(boolean isWhitelistActive) {
		whitelistActive = isWhitelistActive;
	}
	
	
}
